package archetypes.vertxapp;

import io.github.cdimascio.dotenv.Dotenv;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.redis.RedisSessionStore;
import io.vertx.redis.client.Redis;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import archetypes.vertxapp.http.handlers.DemoHttpHandler;
import archetypes.vertxapp.http.handlers.VertxHttpHandler;

@Configuration
public class Config {

  private static final Logger log = LoggerFactory.getLogger(Config.class);

  @Bean
  public String foo(Dotenv dotenv) {
    return dotenv.get("FOO");
  }

  @Bean
  public Dotenv dotEnv() {
    return Dotenv.configure().ignoreIfMissing().ignoreIfMalformed().load();
  }

  // http

  @Bean
  public String corsOriginPattern(Dotenv dotenv) {
    return dotenv.get("CORS_ORIGIN_PATTERN");
  }

  @Bean
  public Vertx vertx() {
    return Vertx.vertx();
  }

  @Bean
  public Redis sessionClient(Vertx vertx, Dotenv dotenv) {
    var redisSessionConString = dotenv.get("SESSION_REDIS_CONNECTION");
    log.info("SESSION_REDIS_CONNECTION: {}", redisSessionConString);
    return Redis.createClient(vertx, redisSessionConString);
  }

  @Bean
  public Router mainRouter(Vertx vertx, String corsOriginPattern, Redis sessionClient) {
    var mainRouter = Router.router(vertx);

    // cors
    mainRouter.route().handler(
        CorsHandler.create(corsOriginPattern)
            .allowCredentials(true)
            .allowedHeaders(
                Set.of("Access-Control-Request-Method", "Access-Control-Allow-Credentials",
                    "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type",
                    "origin", "accept", "x-requested-with"))
            .allowedMethods(
                Set.of(HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT,
                    HttpMethod.DELETE, HttpMethod.OPTIONS, HttpMethod.PATCH)));

    // redis http session

    var sessionStore = RedisSessionStore.create(vertx, sessionClient);
    var sessionHandler = SessionHandler.create(sessionStore);
    mainRouter.route().handler(sessionHandler);

    return mainRouter;
  }

  @Bean
  public Router routerV1(Vertx vertx, Router mainRouter) {
    var routerV1 = Router.router(vertx);
    mainRouter.mountSubRouter("/api/v1", routerV1);
    return routerV1;
  }

  // handlers

  @Bean
  public VertxHttpHandler demoHttpHandler(Vertx vertx, Router routerV1, String foo) {
    var handler = new DemoHttpHandler(foo);
    handler.handle(vertx, routerV1);
    return handler;
  }

}
