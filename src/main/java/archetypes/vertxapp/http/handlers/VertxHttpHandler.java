package archetypes.vertxapp.http.handlers;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;

public interface VertxHttpHandler {

    void handle(Vertx vertx, Router ...router);

}
