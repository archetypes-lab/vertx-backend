package archetypes.vertxapp.http.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class DemoHttpHandler implements VertxHttpHandler {

    private static final Logger log = LoggerFactory.getLogger(DemoHttpHandler.class);

    private final String foo;

    public DemoHttpHandler(String foo) {
        this.foo = foo;
    }

    @Override
    public void handle(Vertx vertx, Router ...router) {

        var subRouter = Router.router(vertx);
        router[0].mountSubRouter("/hello", subRouter);

        subRouter.route("/")
                .method(HttpMethod.GET)
                .handler(this::demoHandler);

        subRouter.route("/person/:personName")
                .method(HttpMethod.GET)
                .handler(this::personName);
        
    }

    private void demoHandler(RoutingContext ctx) {

        log.debug("new request!");

        // Get the address of the request
        String address = ctx.request().connection().remoteAddress().toString();
        // Get the query parameter "name"
        var queryParams = ctx.queryParams();
        String name = queryParams.contains("name") ? queryParams.get("name") : "unknown";
        // Write a json response
        ctx.json(
                new JsonObject()
                        .put("name", name)
                        .put("address", address)
                        .put("message", String.format("Hello %s connected from %s !!", name, address))
                        .put("foo", foo));

    }

    private void personName(RoutingContext ctx) {
        var personName = ctx.pathParam("personName");
        ctx.json(
                new JsonObject()
                        .put("greetings", String.format("Hello World %s", personName)));
    }

}
