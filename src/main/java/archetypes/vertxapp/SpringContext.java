package archetypes.vertxapp;

import org.springframework.context.ApplicationContext;

public class SpringContext {
  private static ApplicationContext context;

  private SpringContext() {
  }

  public static void set(ApplicationContext context) {
    SpringContext.context = context;
  }

  public static <T> T getBean(String name, Class<T> requiredType) {
    return context.getBean(name, requiredType);
  }

  public static <T> T getBean(Class<T> requiredType) {
    return context.getBean(requiredType);
  }

}

