package archetypes.vertxapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

public class Main {

  private static final Logger log = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {

    log.info("Application start!!!");

    // minimal spring IoC Container
    var spring = new AnnotationConfigApplicationContext();
    spring.scan("archetypes.vertxapp");
    spring.refresh();

    // Exposition of the IoC Container
    SpringContext.set(spring);

    // VertX
    var vertx = SpringContext.getBean(Vertx.class);

    vertx.deployVerticle(HttpVerticle.class.getName(),
        new DeploymentOptions().setInstances(Runtime.getRuntime().availableProcessors() * 2));

  }

}
