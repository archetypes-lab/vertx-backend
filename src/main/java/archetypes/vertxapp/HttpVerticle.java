package archetypes.vertxapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;

public class HttpVerticle extends AbstractVerticle {

  private static final Logger log = LoggerFactory.getLogger(HttpVerticle.class);

  @Override
  public void start() {

    log.info("starting new instance of HttpVerticle");

    // Create a Router
    var router = SpringContext.getBean("mainRouter", Router.class);

    // Create the HTTP server
    vertx.createHttpServer()
        // Handle every request using the router
        .requestHandler(router)
        // Start listening
        .listen(8888)
        // Print the port
        .onSuccess(server -> log.info("HTTP server started on port {}", server.actualPort()));
  }
}
