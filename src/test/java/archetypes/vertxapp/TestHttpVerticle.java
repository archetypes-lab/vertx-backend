package archetypes.vertxapp;

import io.vertx.core.Vertx;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@ExtendWith(VertxExtension.class)
class TestHttpVerticle {

  @BeforeEach
  void deploy_verticle(Vertx vertx, VertxTestContext testContext) {

    var spring = new AnnotationConfigApplicationContext();
    spring.scan("archetypes.vertxapp");
    spring.refresh();

    // Exposition of the IoC Container
    SpringContext.set(spring);

    vertx.deployVerticle(new HttpVerticle(), testContext.succeeding(id -> testContext.completeNow()));
  }

  @Test
  void verticle_deployed(Vertx vertx, VertxTestContext testContext) throws Throwable {
    testContext.completeNow();
    Assertions.assertTrue(true);
  }
}
